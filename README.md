# NZ_workplace_structures

This repository holds data on workplaces structures obtained from the IDI. Using IRD tax records for census month in 2018 (March), we provide SA2 level counts of workplaces and their size (i.e., number of employees) by industry sector (using ANZSIC06 codes).

## Instructions for workplace structures node list:
- Clone repo: [https://gitlab.com/cma-public-projects/nz_workplace_structures](url)
- Open `nz_workplace_structures.Rproj`. This will open in RStudio. The `.Rproj` file ensures that all specified paths are relative to the main repo directory.
- Within RStudio, open `processing/construct_workplace_nodelist.Rmd`. This notebook will call all other code files and should run from start to finish to produce the node list.
- Ensure that all R packages are installed. these include: `here`,`tidyverse`,`splitstackshape`,`git2r`, `EnvStats`, `poweRlaw` .
- Set your local path to the dropbox folder `covid-19-sharedFiles/`. Ensure that the final backslash is included. After setting this string, the input files should all have the correct file names. 
- Check the file names (lines 49-58). Ensure that the files have been made available offline on dropbox. If they are stored as "online-only" it will cause an error later on. 
- After all of these steps, you should be able to `Run All` chunks (this can be done by navigating to the top right where it says `Run`, and clicking `Run All` on the drop down menu. The code will take roughly 30-60 minutes to finish (depending on hardware), and it should produce a folder with the node list and sense checking files in an `outputs` folder in the repo, as well as a copy that goes to the `workplaces/nodelists/` folder in dropbox. 
- Double check the node list and the associated sense checking files to make sure the file looks reasonable before using. 
- Additional comments and descriptions of functions are described in `construct_workplace_nodelist.Rmd`.

## instructions for cleaning workplace commute data:
- From the same repo ([https://gitlab.com/cma-public-projects/nz_workplace_structures](url)), open the file `clean_commuting_work.Rmd`.
- Ensure the packages `tidyverse`,`here` are installed.
- Set the local directory for Dropbox on line 34.
- Run notebook by selecting `Run All` in the top right of RStudio.
- Output files will be stored in dropbox: `data/IDI_processed_outputs/commuting/` 

Information on specific folders are detailed as follows:

## Folders

### processing

The ` processing` folder contains the original SQL and R scripts used to extract data from the IDI.

It also includes the code to produce a synthetic set of individual workplaces based on the IDI data. 

- **`construct_workplace_nodelist.rmd`** seeks to use IDI data at SA2 level to generate an SA2 level node list. This is limited in the fact that the SA2 level input data has more suppressed values (~33% of rows compared to around 1.5% of rows at TA level). However we can draw upon TA level data to assist in the imputation by calculating the difference in total counts of workplaces and workers in the SA2-level data compared to the TA level data. We can allocate this difference across randomly across SA2s that have suppressed values to make the total counts more accurate at SA2 level, even though the resulting imputed data itself cannot be considered representative at an SA2-level. This notebook carries out the following steps:
	+ **Retrieval**.  After manually setting the string for the dropbox directory, this phase will copy over relevant files from dropbox to `inputs`.
	+ **Processing**. After files are stored in `inputs`, processing will clean and tidy these files, and then use the functions in the files  `workplace_powerlaw_distribution.R` and `impute_workplaces.R` to impute missing values and produce a synthetic node list.
	+ **Publish**. Once a synthetic node list has been generated, the publish step will automatically copy over the output to the dropbox at `covid-19-sharedFiles\data\workforce\nodelists`. 

- **Edit: Deprecated file.** `Workplace_SA2_Nodelist_V2_Safe.rmd` has been replaced by `construct_workplace_nodelist.Rmd`.


### data (deprecated)

All input files are now copied into the `inputs` folder. `data` is not used. 

### analysis

The `analysis` folder contains any code used for subsequent analysis of data, such as summary statistics or maps. 

### inputs

The `inputs` folder contains data extracted from the IDI as well as other inputs used in processing. Each of the IDI files has been checked by Stats NZ and contains no identifying information. Counts have been aggregated to ensure cell sizes are above 3 for entities (i.e. businesses) and above 5 for individuals, and randomly rounded to base 3. In the cases where aggregation was not sufficient to increase cell sizes, values are suppressed. 

Following the implementation of the retrieval, processing, publish steps in `construct_workplace_nodelist.Rmd`, input data is not stored in the git repo, but automatically copied over from dropbox by in the retrieval phase.

`git_lfs` is implemented for this repo, so ensure that any large files that are to be pushed are tracked. 

### outputs

The `outputs` folder will be the target folder for any results saved by `processing` and `analysis` files. 
Node list output will automatically be published to the dropbox at `covid-19-sharedFiles\data\workforce\nodelists` but will be available to view in `outputs`.

## Authors and acknowledgment

Authors (order alphabetical): James Gilmour, Emily Harvey, Dion O'Neale, Steven Turnbull

We would like to acknowledge the help of Statistics NZ, who processed the data. We would also like to acknowledge the help of Adrian Ortiz-Cervantes. This work was funded and supported by Te P\={u}naha Matatini and MBIE.

## License

Shield: [![CC BY-NC-SA 4.0][cc-by-nc-sa-shield]][cc-by-nc-sa]

This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0
International License][cc-by-nc-sa].

[![CC BY-NC-SA 4.0][cc-by-nc-sa-image]][cc-by-nc-sa]

[cc-by-nc-sa]: http://creativecommons.org/licenses/by-nc-sa/4.0/
[cc-by-nc-sa-image]: https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png
[cc-by-nc-sa-shield]: https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg


## Disclaimer

The results in this paper are not official statistics. They have been created for research purposes from the Integrated Data Infrastructure (IDI), managed by Statistics New Zealand. The opinions, findings, recommendations, and conclusions expressed in this paper are those of the author(s), not Statistics NZ. Access to the anonymised data used in this study was provided by Statistics NZ under the security and confidentiality provisions of the Statistics Act 1975. Only people authorised by the Statistics Act 1975 are allowed to see data about a particular person, household, business, or organisation, and the results in this paper have been confidentialised to protect these groups from identification and to keep their data safe. Careful consideration has been given to the privacy, security, and confidentiality issues associated with using administrative and survey data in the IDI. Further detail can be found in the Privacy impact assessment for the Integrated Data Infrastructure available from www.stats.govt.nz. 
