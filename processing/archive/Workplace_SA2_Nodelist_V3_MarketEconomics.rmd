---
title: "Workplace_SA2_Nodelist_V3_MarketEconomics"
author: "Steven Turnbull"
date: "22/02/2022"
---

This notebook will build the workplace node list, using NZ level data on workplaces sizes from the IDI to obtain a distribution of workplace sizes, and then use SA2 level data on number of workplaces (Geographical Units) and Employees from Market Economics.  
Always make sure to open the `.Rproj` file located prior to running, as this will make the `here()` library point to the correct directories when loading inputs and writing outputs. 

# libraries
```{R}
#first load in relevant libraries
library(here)
library(tidyverse)
library(splitstackshape)
```

```{R}
message(paste("The working directory found by the here function is:", here()))
message(paste("All inputs should be put into the folder (and it's subdirectories)", here("inputs")))
message(paste("All outputs should be saved into the folder (and it's subdirectories)", here("outputs")))
```

```{R}
#load nz distribution of workplace sizes
#we will use this NZ level distribution to help impute individual workplace sizes.
nz_distribution <- read_csv(here("data","df_firmsizes_nz_safe.csv")) %>% 
  #get a minimum and maximum size where only a range is given.
  mutate(min_size= parse_number(str_extract(Firm_size, pattern = "(.*)_"))) %>%
  mutate(max_size= parse_number(str_replace(Firm_size, pattern = ".*_(.*)$",replacement = "\\1"))) %>% 
  #we will disaggregate the rows, so the count of workplaces is represented by rows representing individual workplaces
  expandRows("Count_PBN") %>%
  #for workplace rows where a range is given, we will fill in ranged Firm_size with a random value in that range
  #this will give a "complete" synthetic set of individual workplaces at the NZ level.
  rowwise() %>%
  mutate(Firm_size = ifelse(str_detect(Firm_size,"_to_"),
                                           round(sample(min_size:max_size,1)),
                                           parse_number(Firm_size))
                                ) %>% 
  #We can now group by firm size can calculate the counts of each specific size
  group_by(Firm_size) %>%
  summarise(Count_PBN = n()) %>%
  select(Firm_size,Count_PBN)  %>%
  #and calculate the probability of each specific size.
  mutate(prob = Count_PBN/sum(Count_PBN))
 
#plot distribution
ggplot(nz_distribution,aes(y=Count_PBN,x=Firm_size)) + geom_point()+ scale_x_log10() + scale_y_log10()
```

```{R}
#Our goal is to impute unknown workplace sizes for the TA/Sector level data when we are only given a range of values. 
#Following the NZ level distribution, a power law distribution provides a good fit to what would be expected (smaller workplace sizes have higher probability, with this probability conistently decreasing as workplace size increases)
# We can use this information to provide a distribution to sample from when estimating a set of workplace sizes when only a range of values is given.
#We'll use the poweRlaw package to calculate the relevant information for creating this distribution.
#Gillespie, C. S. “Fitting heavy tailed distributions: the poweRlaw package.” Journal of Statistical Software, 64(2) 2015.
library(poweRlaw)
#get complete vector of firm sizes
data <- nz_distribution %>% expandRows("Count_PBN") %>% select(Firm_size) %>% pull()

#fit powerlaw model to data
m_pl <- displ$new(data)
est_pl <- estimate_xmin(m_pl)
#xmin
est_pl$xmin
#gamma
est_pl$pars
#goodness of fit
est_pl$gof

#set xmin for powerlaw model
m_pl$setXmin(est_pl)

#lognorm
# m_ln = dislnorm$new(data)
#   est_ln = estimate_xmin(m_ln)
#   est_ln$pars = signif(est_ln$pars, 3)
#   m_ln$setXmin(est_ln)
#   
# #poisson
# m_pois = dispois$new(data)
#   est_pois = estimate_xmin(m_pois)
#   m_pois$setXmin(est_pois)

```

```{R}
#plot fit
plot(m_pl, xlab = "x", ylab = "CDF",
     panel.first = grid(col = "grey80"),
     pch = 21, bg = 1)

lines(m_pl, col = 2, lwd = 2)
#lines(m_ln, col = 3, lwd = 2)
#lines(m_pois, col = 4, lwd = 2)

```

```{R}
#concordance
SA2_TA_concordance <- read_csv(here("inputs","SA2_TA18_concordance.csv"))

#get market economics data
ME_df <- read_csv(here("data","BD data output 2018.csv")) %>%
  rename(Sector = "ANZSIC06_1D_Code") %>%
  #round counts
  mutate_at(.vars = vars(GEO_2018,EC_2018,MEC_2018),
            .funs = list(~round(.))) %>%
   #add SA22018 code and TA from concordance
  left_join(SA2_TA_concordance, by = c("SA22018_name"="SA22018__1")) %>%
  #get count of difference between the employee count (EC) and the modified employee count (MEC)
  #mutate(Diff = MEC_2018-EC_2018) %>%
  #Take the difference away from the Geogrpahical units (GEOS) as these likely represent single employee businesses/other enterprises we're not looking for
  #mutate(Firms = GEO_2018-Diff) %>% 
  #clean TA col
  mutate(TA2018 = str_pad(string = TA2018_V1_,2,"left","0")) %>%
  #select relevant columns
  select(SA22018_V1,TA2018,Sector,GEO_2018,EC_2018,MEC_2018) %>%
  arrange(SA22018_V1,Sector) %>%
  #create a min size and max size for each SA2/Sector
  mutate(min_size =  1) %>%
  #make max size be the number of employees minus workplaces, unless the number of workplaces is greater than number of employees
  mutate(max_size = ifelse(GEO_2018<EC_2018, EC_2018  - GEO_2018, 1)) %>%
  #if there's only 1 workplace, max size will be equal to number of employees
  mutate(max_size = ifelse(GEO_2018==1, EC_2018, max_size)) %>%
  #remove rows with 0 firms or employees
  filter(GEO_2018 !=0, EC_2018!=0)

```

#work out distribution for workplace data
```{R}
#The distribution we will draw from to impute workplace sizes will be made from a combination of the empirical data, and values drawn from the powerlaw above the point (xmin) that makes sense. This is possible if xmin is located within the empirical data where ranges are granular.
#for example, the nz level data provides granular sizes up to around size 220, while the powerlaw package detects xmin around 164.
#we will create a data frame of workplace sizes from 1 to 6000, with an associated probability. 
#The probability for workplaces sizes smaller than xmin will be derived from the empirical data, while the other workplaces sizes will have a probability derived from the powerlaw estimate.

#get the estimated probabilities from the powerlaw model - this will be n-xmin in length
estimated_probs_pl <- dist_pdf(m_pl, c(est_pl$xmin:6000))
#tidy estimated probabilities into data frame 
estimated_distribution<- data.frame(Firm_size = est_pl$xmin:6000,
                               prob = estimated_probs_pl)

#get the empirical probabilities from 1:xmin
empirical_distribution <- nz_distribution %>% filter(Firm_size <est_pl$xmin) %>% select(-Count_PBN)

#sum of probabilities for empirical data (i.e., small firms < xmin)
Psmall <- sum(empirical_distribution$prob)


#sum of probabilties for estimated data (i.e., large firms >= xmin)
Plarge <- sum(estimated_distribution$prob)


#empirical data
new_distribution<- estimated_distribution %>%
  mutate(prob = prob*(1-Psmall)) %>% # this type of rescaling means that we will miss a small bit of probability for firm sizes >6000
  rbind(empirical_distribution) %>%
  arrange(Firm_size)

sum(new_distribution$prob)

#plot showing distribution with source of probabilities for each workplace size.
new_distribution %>%
    mutate(Distribution = ifelse(Firm_size >= est_pl$xmin, "Powerlaw", "Empirical")) %>%
ggplot(aes(x = Firm_size,y = prob, colour = Distribution)) +
  geom_point(stat = "identity") +
  geom_vline(aes(xintercept = est_pl$xmin), colour = "#e34a33") +
  scale_x_log10() + 
  scale_y_log10() +
  annotate("text",x=10,y=0.2,label = "Empirical") +
  annotate("text",x=1500,y=0.000003,label = "Power Law")+
  annotate("text",x=est_pl$xmin+75,y=0.001,label = "x-min")+

  xlab("Workplace Size") +
  scale_color_brewer(palette = "Paired") +
  ylab("Probability") +
  theme_bw() +
  theme(legend.position = "none")
ggsave(here("outputs","Powerlaw_distribution_workplaces.pdf"))
```

```{R}
ME_df 
```

```{R}
# We will now iterate through Territorial Authorities (TA) and industry Sectors, making an estimate of how big each individual workplace would be based on the range of workplace sizes, the total number of people employed by workplaces within that range, and the distribution calculated above.

#first, disaggregate the table of counts so that each row represents an individual workplace.
workplace_nodelist_expanded <- ME_df %>%
  expandRows(count = "GEO_2018")
```

```{R}
#create vector where new firm size values will be stored
allocated_Firm_size_df <- data.frame()

#get the number of TAs. this will be used to calculate progress.
SA2_length <- length(unique(ME_df$SA22018_V1))

#time it
Time1 <- Sys.time()
for(SA2_i in 1:SA2_length){
  #for a specific TA
  SA2 <- unique(ME_df$SA22018_V1)[SA2_i]
  #SA2<-100100
  SA2_df <- workplace_nodelist_expanded %>%
        filter(SA22018_V1 == SA2)
  
  #for an industry sector within that TA
  for(SECTOR in unique(SA2_df$Sector)){
    #SECTOR <- "A"
    Sector_SA2_df <- SA2_df %>% filter(Sector == SECTOR)
    
      
      #assign total count of workers who are employed in workplaces of that size to M
      M <- first(Sector_SA2_df$EC_2018)
      #assign number of firms of that size to N
      N <- nrow(Sector_SA2_df)
      #We want to allocate the M number of workers across the N number of workplaces, resulting in the correct distribution
      
      #if that combination of TA, SECTOR, and SIZE exist in our data..
      if(nrow(Sector_SA2_df)>0){
        #start sampling to fill firms with workers
        #since we have a criteria to meet, we will only close this iteration when the criteria is met.
        #until this, we will set a this to be false.
        criteria_met <- FALSE
        
        #count how many iterations, if this get's too high, we will adjust so the code does not get stuck.
        counter <- 0
        #create data_frame for storing vectors of possible workplace sizes.
        #We wanto to record all the guesses, so that if the counter gets too high we can take the closest effort and work with it.
        vec_storage <- data.frame(matrix(ncol=2+N,
                                         nrow=1, 
                                         dimnames=list(NULL,
                                                       c("Counter","Sum_vec",paste0("Cell",1:N))
                                                       )
                                          ))
        #if we know the firm size (min and max are the same), then we can create the vector easy
        # M workers will be equal to N*workplace size. 
        if(first(Sector_SA2_df$min_size) == first(Sector_SA2_df$max_size)){
          vec <- Sector_SA2_df$min_size
          criteria_met <- TRUE
        }
        
        while(criteria_met == FALSE){
          #add to counter
          counter <- counter+1

          #if there's only one firm in that group, assign value as the count of workers
          #(in these cases we don't need to distribute a count, since there's only 1 firm that can take the workers)
          if(N == 1){
            vec <- M[[1]]
            criteria_met <- TRUE
          }else{ 
            # if there are more than 1 firm present, we will need to allocate the M count of people across the N count of workplaces.
            #first get the distribution for range of firm sizes in that group
            bag_Firm_sizes <- new_distribution %>%
              filter(between(Firm_size,first(Sector_SA2_df$min_size), first(Sector_SA2_df$max_size)))
            
            #sample from the distribution to create vector of firm sizes
            vec <- sample(bag_Firm_sizes$Firm_size, prob = bag_Firm_sizes$prob, size =N, replace = T)
            
            #store vector to main dataframe of vectors
            #this is where we store all of the guesses we make from that sample
            #if there have been many iterations (1000), we will stop sampling, and work with the closest guess.
            vec_storage<-rbind(vec_storage, c(counter,sum(vec),vec))
            
            #if the counter gets too high, we'll need to brute force a match using the best guess
            #we can do this by taking the vector that most closely sums to the count of workers, and perturbing the individual sizes
            if(counter == 10){
              #print("Tried our best, now forcing the distribution to match")
              best_vec <- vec_storage %>%
                #remove the test row made when creating the dataframe
                filter(!is.na(Counter)) %>%
                #calculate the difference between each the sum of each guess, and the actual number of workers needed to cover.
                mutate(Difference = M-Sum_vec) %>%
                #filter to vector with the minimum absolute difference between sum of sampled vector and count of workers
                filter(abs(Difference) == min(abs(Difference))) %>%
                #take the first result
                head(1) %>%
                #grab the vector
                select(matches("Cell")) %>% unlist()
              
              Difference <- M-sum(best_vec)
              #for each individual discrepancy between that guess and the M number of workers,
              #we will add/substract a worker randomly from firms until no more difference
              for(. in seq_len(abs(Difference))){
              best_vec[i] <- best_vec[i <- sample(N, 1)] + sign(Difference) #(this will +/- 1 to a random firm)
              }
              #while any firm size is negative or 0, we will reassign count to another firm
              
              #create a second counter to assess any issues running next section
              second_counter <- 0
              #if number of firms is greater than count of workers, there is going to be at least one firm allocated a size 0.
              if(N>M){
              while (any(best_vec < 0)) {
              negs <- best_vec <= 0
              pos  <- best_vec > 0
              best_vec[negs][i] <- best_vec[negs][i <- sample(sum(negs), 1)] + 1
              best_vec[pos][i]  <- best_vec[pos ][i <- sample(sum(pos ), 1)] - 1
              second_counter <- second_counter+1
              
              #print warning if this while statement goes on too long
              if(second_counter == 10000){print(paste("Stuck in a loop on",SA2,SECTOR,SIZE))}
              }
                #if the number of firms is less than the number of workers, we should be able to avoid firms sized 0.
                }else{
              #We could refine this even further by changing the conditional to ensure elements of best_vec are within correct range 
              while (any(best_vec <= 0)) {
              negs <- best_vec <= 0 #negs will now include 0
              pos  <- best_vec > 1 #only sample from firms which can afford to drop one person
              best_vec[negs][i] <- best_vec[negs][i <- sample(sum(negs), 1)] + 1
              best_vec[pos][i]  <- best_vec[pos ][i <- sample(sum(pos ), 1)] - 1
              
              second_counter <- second_counter+1
              #print warning if this while statement goes on too long
              if(second_counter == 10000){print(paste("Stuck in a loop on",SA2,SECTOR))}
              }
                  }
              
              vec <- best_vec
            }
            
             #if this vector of firm sizes matches the count - Success
            if(sum(vec)==M){criteria_met <- TRUE}   #otherwise it will repeat the process
          
        }
      
        }
         #append successful vector of firm sizes to main list
        allocated_Firm_size_df  <- bind_rows(allocated_Firm_size_df,Sector_SA2_df %>% mutate(allocated_Firm_size = vec))
    }
  }
  
  #print summary at every 10% complete
  if(SA2_i%%round(SA2_length/10)==0){
    print(paste0("Loop ",SA2_i,": Finished SA2 ", SA2))}
  if(SA2_i%%round(SA2_length/10)==0){
    print(paste0("Time elapsed: ", round(Sys.time()-Time1,2),
                             ". ", round((SA2_i/SA2_length)*100), " Percent complete."))
    }
               
}
```

```{R}
#what's the sum of the new allocated_Firm_size? Does it equal sum(Count_people) in original data
sum(allocated_Firm_size_df$allocated_Firm_size) == sum(ME_df$EC_2018) #should be TRUE
#how many allocated firm sizes fit the desired range
allocated_Firm_size_df %>%
  mutate(fit_range = ifelse(allocated_Firm_size >=min_size & allocated_Firm_size <= max_size, TRUE,FALSE)) %>%
  summarise(range_test = sum(fit_range)/nrow(.))

allocated_Firm_size_df %>%
    mutate(fit_range = ifelse(allocated_Firm_size >=min_size & allocated_Firm_size <= max_size, TRUE,FALSE)) %>%
group_by(SA22018_V1,Sector) %>%
  filter(any(fit_range == F))

#any size 0?
allocated_Firm_size_df %>% filter(allocated_Firm_size==0)
```


# save 
```{R}
workplace_nodelist_complete <- allocated_Firm_size_df %>%
  mutate(Firm_size = allocated_Firm_size) %>%
  #add workplace ID
  mutate(Workplace_ID = paste0("WP",
                               str_pad(as.character(TA2018),width = 2,side = "left",pad = 0),
                               str_pad(as.character(row.names(.)), width = 7, side="left",pad=0)
                               ))  %>%
  select(Workplace_ID,SA22018_V1, TA2018, Sector, Firm_size) %>%
  rename(firm_size="Firm_size",
         SA22018 = "SA22018_V1")

write_csv(workplace_nodelist_complete,here("outputs","Workplace_SA2_nodelist_V3_MarketEconomics.csv"))
write_csv(workplace_nodelist_complete,here("outputs",paste0(Sys.Date(),"-Workplace_SA2_nodelist_V3_MarketEconomics.csv")))


workplace_nodelist_complete %>% filter(firm_size!=1)

```


```{R}
read_csv(here("outputs","Workplace_SA2_nodelist_V3_MarketEconomics.csv")) %>%
  group_by(firm_size) %>%
  summarise(Count_PBN = n_distinct(Workplace_ID)) %>%
ggplot() + 
  geom_point(aes(y=Count_PBN,x=firm_size, colour = "Market Economics"),alpha=0.8)+

  geom_point(data = nz_distribution, aes(y=Count_PBN,x=Firm_size,colour="NZ"),alpha=0.8)+
  
  scale_x_log10() + scale_y_log10() +
  xlab("Workplace Size") +
  ylab("Count of Workplaces") +
  theme_bw() +
  scale_color_manual("Distribution",values = c("#af8dc3","#7fbf7b"))

ggsave(here("outputs","NZ_Market_Economics_distribution_comparison_.pdf"))
```


