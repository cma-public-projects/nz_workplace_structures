IDI data were obtained through .SQL which were not released with the extraction. 
The dataProposal provides a summary of how the workplace sizes table were designed and extracted

# Processing Workplace Files
Author Steven Turnbull (s.turnbull@auckland.ac.nz)

The processing code contains one notebook that will build a workplace node list, using SA2 level data on workplaces sizes from the IDI, as well as a notebook for processing/cleaning workplace commuting data from the IDI.

Always make sure to open the `.Rproj` file located prior to running, as this will make the `here()` library point to the correct directories when loading inputs and writing outputs. 

## construct_workplace_nodelist.Rmd
This notebook has 3 key steps:
- **Retrieval**. All files used as input are copied over to the `inputs` folder from Dropbox.

- **Processing**. Inputs are loaded in and processed to construct the node list. Processing will carry out the following steps:
1) Load input and clean files.
2) Impute missing values for:
  a) Count_people (the number of people) at workplaces for each firm size band, sector, and SA2. 
  b) Count_PBNs (the number of permanent business numbers, aka firms) for each firm size band, sector, and SA2. 

  c) Workplace sizes that have been aggregated within a range of possible values. For example, if we have 10 PBNs with 50 people within a firm size band of 5-9, each of the 10 PBN workplaces will be allocated a number of staff ranging between 5 and 9, such that the total across them sums to 50 people.

3) Add Working Proprietors. We source counts of working proprietors from Stats BD data and append them to the node list. Currently the assumption is made that each of these working proprietor firms is sized 1.

4) Tidy and save final node list to `outputs`.

- **Publish**. The final output is copied over to the relevant location in dropbox.
- The node list output will be stored in a folder named with the latest git commit ID and date with a collection of files that detail a broad level summary of the node list attributes, as well as plots of workplace size distributions as a whole and across TAs and sectors.
