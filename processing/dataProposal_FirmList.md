# Need for the data  

The proposed data extraction seeks to provide a table of firm sizes, with number of firms according to that size and number of employees employed by firms of that size. This table can be expanded once outside the IDI to provide a nodelist of firms with size as an attribute. 

# Structure
Table A - firm
|Firm_size|Number_of_firms|Number_of_workers|
|---|---|---|
|1:209 (fine-grained)  |n|	n |
|210-399 (banded to 5)|n|	n |
|400-599 (banded to 50)     |n| n |
|600-999 (banded to 100)  |n|	n |
|1000-2499 (banded to 500)  |n| n |
|2500+ (banded to 1000)|n| n |
|…      |…|…       |
Where Columns A refers to different firm sizes, and column B and D refers to a count of the firms per firm size and the count of the workers per firm size respectively. To ensure that the optimal amount of information can be released without suppression, we use a aggregation on firm size which changes as the distribution of firm sizes becomes more sparse. For example, for firm sizes lower than 210, the number of firms and number of workers is sufficient to be extracted, but we need to increasingly expand the size of the bands in which firms are binned. For example, there a very few firms with a size over 2500 workers, so these need to be grouped together.   



Also do for sector and geospatial levels TA. 

# Implementation 
We draw data through SQL from the IR.EMS table joined to the PBR tableto provide information on the firms present in tax data in 2018 census month. Data quality checks are made in R, and then we use a separate R notebook to aggregate and confidentialise data for extraction. 

# Use:
We will use the table to produce the workplace nodelist by expanding the table.

# Future work:
We could look at automating the optimisation of firm size bands. Currently the bands are determined manually. 

We need to double check data quality and see how the firm list matches up with different years. We will extract the firm list based on March 2018 so it matches census, but we would expect 2020 to look much different. 