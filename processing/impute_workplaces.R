impute_TA_workplaces_count_people <- function(workplaces_sector_sa2, workplaces_sector_ta, new_distribution){
  
  #First impute missing values at SA2 level using known counts from TA.
  #We will allocate counts of people across SA2s with suppressed workplaces counts. 
  #iterate through TAs and Sectors to allocate missing people across suppressed SA2s.
  #We will store them in this data.frame
  workplaces_sector_sa2_imputed <- data.frame() 
  
  #get the number of TAs. this will be used to calculate progress.
  TA_length <- length(unique(workplaces_sector_sa2$TA2018))
  #time it
  Time1 <- Sys.time()
  for(TA_i in 1:TA_length){
    #for a specific TA
    TA <- unique(workplaces_sector_sa2$TA2018)[TA_i]
    workplaces_sa2_ta_df <- workplaces_sector_sa2 %>%
      filter(TA2018 == TA)
    
    #for an industry sector within that TA
    for(SECTOR in unique(workplaces_sa2_ta_df$Sector_anzsic06)){
      #filter to TA and Sector
      workplaces_SA2_group <- workplaces_sa2_ta_df %>%
        filter(TA2018==TA, Sector_anzsic06==SECTOR) %>%
        mutate(Count_PBN = ifelse(Count_PBN == -999999,NA,Count_PBN),
               Count_people = ifelse(Count_people == -999999,NA,Count_people))
      
      workplaces_TA_group <- workplaces_sector_ta %>%
        filter(ta_code==TA, Sector_anzsic06==SECTOR)
      
      #what's the difference in PBNs and people? This difference needs to be allocated across suppressed SA2s
      workplaces_PBN_diff <- sum(workplaces_TA_group$Count_PBN) - sum(workplaces_SA2_group$Count_PBN,na.rm=T)
      workplaces_people_diff <- sum(workplaces_TA_group$Count_people) - sum(workplaces_SA2_group$Count_people,na.rm=T)
      
      #print(paste0("On sector ", SECTOR))
      #if there's suppressed SA2s, we will seek to impute populations
      if(sum(is.na(workplaces_SA2_group$Count_PBN))>0){
        #if there's not any people to add, we will just impute the suppressed value as 1.
        if(workplaces_people_diff <0){
          vec <- workplaces_SA2_group %>%
            #for each suppressed SA2
            filter(is.na(Count_PBN)) %>%
            mutate(vec = 1)%>%
            select(vec) %>% pull()
          #Otherwise will will allocate the people that need to be added across the suppressed SA2s.
        }else{
          #print(paste0("There are people to add and suppressed SA2s"))
          #we want to allocate the workers present in the TA level data who are missing from the SA2 level data 
          #(call this number M) across suppressed SA2s.
          M <- workplaces_people_diff
          
          #We will allocate M across the number of suppressed SA2s (call this N)
          N <- sum(is.na(workplaces_SA2_group$Count_PBN))
          
          #before we start, count how many iterations - if this get's too high, we will adjust so the code does not get stuck.
          counter <- 0
          #code will keep running until the following criteria is met
          # - the new vector of counts (call this "vec") equals the count of M people.
          #We'll store guess in a dataframe called "Vec_storage"
          Vec_storage <- workplaces_SA2_group %>%
            #for each suppressed SA2
            filter(is.na(Count_PBN)) %>%
            #Record counter
            mutate(Counter = NA) %>%
            mutate(vec = NA) %>%
            select(Counter, everything()) %>%
            mutate(in_range = NA)
          
          criteria_met <- FALSE
          while(criteria_met == FALSE){
            #print(paste0("Starting imputation"))
            
            #add to counter
            counter <- counter+1
            
            #if there's only one suppressed SA2 in that group, assign value as the count of people as M.
            #(in these cases we don't need to distribute a count, since there's only 1 possible sa2 that can take people)
            if(N == 1){
              vec <- M[[1]]
              criteria_met <- TRUE
            }else{ 
              
              # if there are more than 1 suppressed SA2 present, we will need to allocate the M count of people across the N suppressed SA2s
              #first create vector of firm sizes for the suppressed SA2s using the sample function.
              vec_df <- workplaces_SA2_group %>%
                #for each suppressed SA2
                filter(is.na(Count_PBN)) %>%
                #record iteration
                mutate(Counter = counter) %>%
                #add relevant probabilities from powerlaw distribution per case
                rowwise() %>%
                mutate(Probs = list(new_distribution %>% filter(Firm_size>=min_size&Firm_size<=max_size) %>% select(prob) %>% pull())) %>%
                mutate(vec = sample(min_size:max_size,1,prob=unlist(Probs))) %>%
                ungroup() %>% select(-Probs)
              #grab just the values of possible counts
              vec <- vec_df %>% select(vec) %>% pull()
              #if the sample does not sum to the population, add/subtract the difference
              if(sum(vec) != M){
                #print(paste0("Adding/Substracting people"))
                #what's the difference between the count of people and our vector of sampled firm sizes
                Difference <- M-sum(vec)
                #for each individual discrepancy between that guess and the M number of workers,
                #we will add/substract a worker randomly from firms until no more difference
                for(. in seq_len(abs(Difference))){
                  vec[i] <- vec[i <- sample(N, 1)] + sign(Difference) #(this will +/- 1 to a random firm)
                }
                #while any count is negative or 0, we will reassign count to another SA2
                
                #create a second counter to assess any issues running next section
                second_counter <- 0
                #if number of suppressed SA2s is greater than count of people, there is going to be at least one SA2 allocated a size 0.
                if(N>M){
                  while (any(vec < 0)) {
                    #print(paste0("Shuffling to remove negative counts"))
                    negs <- vec <= 0
                    pos  <- vec > 0
                    vec[negs][i] <- vec[negs][i <- sample(sum(negs), 1)] + 1
                    vec[pos][i]  <- vec[pos ][i <- sample(sum(pos ), 1)] - 1
                    second_counter <- second_counter+1
                    
                    #print warning if this while statement goes on too long
                    if(second_counter == 10000){print(paste("Stuck in a loop on",TA,SECTOR))}
                  }
                  #if the number of SA2s is less than the number of people, we should be able to avoid SA2s sized 0.
                }else{
                  #print(paste0("Shuffling to remove zero counts"))
                  #We could refine this even further by changing the conditional to ensure elements of best_vec are within correct range 
                  while (any(vec <= 0)) {
                    negs <- vec <= 0 #negs will now include 0
                    pos  <- vec > 1 #only sample from SA2s which can afford to drop one person
                    vec[negs][i] <- vec[negs][i <- sample(sum(negs), 1)] + 1
                    vec[pos][i]  <- vec[pos ][i <- sample(sum(pos ), 1)] - 1
                    
                    second_counter <- second_counter+1
                    #print warning if this while statement goes on too long
                    if(second_counter == 10000){print(paste("Stuck in a loop on",TA,SECTOR))}
                  }
                }
              }
              #print(paste0("Checking counts in new vector are in specified ranges"))
              #apply new vector as a variable to vec storage dataframe 
              vec_df<-vec_df %>%
                #get latest vec
                mutate(vec = vec) %>%
                #and test the values fit within the ranges specified
                mutate(in_range = ifelse(vec>=min_size & vec<=max_size,T,F))
              #if all the counts are within range and the counts add up to the correct total, job done
              if(sum(vec_df$in_range)/nrow(vec_df)==1 & sum(vec)==M){criteria_met <- TRUE}
              
              #if this doesn't work, save the guess and go again.
              Vec_storage <- rbind(Vec_storage,vec_df)
              #if we try lots of times but are unsuccessful, we will pick the best guess so far.
              if(counter == 2){
                #find the guess with the smallest difference compared to the range.
                best_vec <- Vec_storage %>%
                  mutate(Difference = vec-max_size) %>%
                  group_by(Counter) %>%
                  mutate(Total_Difference = sum(Difference)) %>%
                  filter(Total_Difference == min(Total_Difference)) %>%
                  first() %>%
                  ungroup() %>%
                  select(vec) %>% pull()
                #assign the best guess as our new vector of values
                vec <- best_vec
                criteria_met <- TRUE}
              #print warning if this while statement goes on too long
              if(counter == 10000){
                print(paste("Stuck in a loop on",TA,SECTOR))
                stop()
              }
            }
          }
        }
        #store the new vector with the rest of the TA/sector group.
        workplaces_SA2_group[is.na(workplaces_SA2_group$Count_PBN),"Count_people"] <- vec
      }
      #append this TA sector group to the main dataframe
      workplaces_sector_sa2_imputed <- bind_rows(workplaces_sector_sa2_imputed,workplaces_SA2_group)
    }
    #print summary at every 10% complete
    if(TA_i%%round(TA_length/10)==0){
      print(paste0("Loop ",TA_i,": Finished TA ", TA))}
    if(TA_i%%round(TA_length/10)==0){
      print(paste0("Time elapsed: ", round(Sys.time()-Time1,2),
                   ". ", round((TA_i/TA_length)*100), " Percent complete."))
    }
    
  }
  
  print("Finished imputation of Count_people")
  #test the imputed output against the input data 
  #workplaces_sector_sa2 %>%arrange(sa2_code,TA2018,Sector_anzsic06,Firm_size)  
  #workplaces_sector_sa2_imputed %>% arrange(sa2_code,TA2018,Sector_anzsic06,Firm_size)  
  
  #what proportion of imputed people counts fall within the correct range?
  workplaces_sector_sa2_imputed %>% filter(is.na(Count_PBN)) %>%
    mutate(in_range = ifelse(Count_people>=min_size & Count_people<=max_size,T,F)) %>%
    summarise(prop_in_range = sum(in_range/n()))
  
  
  workplaces_sector_sa2_imputed %>%
    filter(is.na(Count_PBN)) %>%
    mutate(diff=Count_people - max_size) %>%
    arrange(desc(diff))
  
  workplaces_sector_sa2_imputed <- workplaces_sector_sa2_imputed %>% rename(SA22018 = "sa2_code")
  #save a version to save running code again if needed
  # write_csv(workplaces_sector_sa2_imputed,
  #           here("outputs","workplaces_sector_sa2_imputed.csv"))
  # 
return(workplaces_sector_sa2_imputed)
}


######################


#function to impute the count of PBNs
impute_TA_workplaces_count_PBN <- function(workplaces_sector_sa2_imputed, workplaces_sector_ta){
  #Impute the count of PBNs. 
  # load the saved version
  workplaces_sector_sa2_imputed <- workplaces_sector_sa2_imputed %>%
    #If sector is NA, replace with character string 
    mutate(Sector_anzsic06 = ifelse(is.na(Sector_anzsic06),"NA",Sector_anzsic06)) %>%
    mutate(TA2018 = ifelse(is.na(TA2018),"NA",TA2018))
  
  #As a base to work off, we will make any suppressed Count_PBN value = 1. The minimum it can be.
  workplaces_sector_sa2_imputed <- workplaces_sector_sa2_imputed %>% 
    mutate(Count_PBN_new = ifelse(is.na(Count_PBN),1, Count_PBN))
  
  #Iterate through TAs and Sectors.
  TA_length <- length(unique(workplaces_sector_sa2_imputed$TA2018))
  #Store imputed data in this dataframe "new"
  workplaces_sector_sa2_imputed_new <- data.frame()
  
  for(TA_i in 1:TA_length){
    #for a specific TA
    TA <- unique(workplaces_sector_sa2_imputed$TA2018)[TA_i]
    workplaces_sa2_ta_df <- workplaces_sector_sa2_imputed %>%
      filter(TA2018 == TA)
    
    #for an industry sector within that TA
    for(SECTOR in unique(workplaces_sa2_ta_df$Sector_anzsic06)){
      #filter to TA and Sector
      workplaces_SA2_group <- workplaces_sa2_ta_df %>%
        filter(TA2018==TA, Sector_anzsic06==SECTOR) 
      
      #filter to this SA2/Sector/sizes cases with suppression
      workplaces_SA2_group_missing <-  workplaces_SA2_group %>% filter(is.na(Count_PBN))
      
      #and the corresponding TA level counts for that TA/sector
      workplaces_TA_group <- workplaces_sector_ta %>%
        filter(ta_code==TA, Sector_anzsic06==SECTOR)
      
      #what's the difference in current count of PBNs in SA2 data compared to TA? We will allocate this difference across the SA2s that were suppressed originally. 
      workplaces_PBN_diff <- sum(workplaces_TA_group$Count_PBN) - sum(workplaces_SA2_group$Count_PBN_new)
      #vec will be the vector of counts for the PBNs. Currently this will just be the guess provided when Count_PBN_new is created above.
      vec <- workplaces_SA2_group_missing$Count_PBN_new
      #If there's a difference in the Count_PBN at TA level, we will distribute them across suppressed SA2 case randomly. 
      #This will update the values in "vec"
      if(workplaces_PBN_diff > nrow(workplaces_SA2_group_missing) & nrow(workplaces_SA2_group_missing)>0){
        for(. in seq_len(abs(workplaces_PBN_diff))){
          vec[i] <- vec[i <- sample(nrow(workplaces_SA2_group_missing), 1)] + sign(workplaces_PBN_diff) #(this will +/- 1 to a random SA2 case)
        }
      }
      
      #store the new vector with the rest of the TA/sector group.
      workplaces_SA2_group[is.na(workplaces_SA2_group$Count_PBN),"Count_PBN"] <- vec
      workplaces_SA2_group <- workplaces_SA2_group %>% select(-Count_PBN_new)
      
      #append this TA sector group to the main dataframe
      workplaces_sector_sa2_imputed_new <- bind_rows(workplaces_sector_sa2_imputed_new,workplaces_SA2_group)   
    }
  }
  
  
  print(paste0("Is the dataframe output of this function the same size as the dataframe that went in? - ",nrow(workplaces_sector_sa2_imputed_new) == nrow(workplaces_sector_sa2_imputed)))
  workplaces_sector_sa2_imputed <- workplaces_sector_sa2_imputed_new
  
  #Summary of differences between the current stage of imputed SA2 data and TA level counts 
  print(paste0("Difference number of workplaces in TA level data (",
               sum(workplaces_sector_ta$Count_PBN),
               ") and SA2 level imputed data (",
               sum(workplaces_sector_sa2_imputed$Count_PBN),
               ") is ",
               sum(workplaces_sector_ta$Count_PBN) - sum(workplaces_sector_sa2_imputed$Count_PBN))
  )
  
  print(paste0("Difference number of workers/jobs in TA level data (",
               sum(workplaces_sector_ta$Count_people),
               ") and SA2 level imputed data (",
               sum(workplaces_sector_sa2_imputed$Count_people),
               ") is ",
               sum(workplaces_sector_ta$Count_people) - sum(workplaces_sector_sa2_imputed$Count_people))
  )
  
  print("Finished imputation of Count_PBN")
  
  
return(workplaces_sector_sa2_imputed)
  
} 


################


impute_workplace_sizes <- function(workplaces_sector_sa2_imputed, new_distribution){
  # We will now iterate through SA2 and industry Sectors, making an estimate of how big each individual workplace would be based on the range of workplace sizes, the total number of people employed by workplaces within that range, and the distribution calculated above.
  #first, disaggregate the table of counts so that each row represents an individual workplace.
  workplace_nodelist_expanded <- workplaces_sector_sa2_imputed %>%
    expandRows(count = "Count_PBN") 
  
  #create vector where new firm size values will be stored
  allocated_Firm_size_df <- data.frame()
  
  #get the number of SA2s. this will be used to calculate progress.
  SA2_length <- length(unique(workplaces_sector_sa2_imputed$SA22018))
  
  #time it
  Time1 <- Sys.time()
  for(SA2_i in 1:SA2_length){
    #for a specific SA2
    SA2 <- unique(workplaces_sector_sa2_imputed$SA22018)[SA2_i]
    SA2_df <- workplace_nodelist_expanded %>%
      filter(SA22018 == SA2)
    #for an industry sector within that SA2
    for(SECTOR in unique(SA2_df$Sector_anzsic06)){
      Sector_SA2_df <- SA2_df %>% filter(Sector_anzsic06 == SECTOR)
      
      #for a workplace size within that SA2 and Sector
      for(SIZE in unique(Sector_SA2_df$Firm_size)){
        #print(paste0("Iterating through workplace size", SIZE,"in sector ",SECTOR))
        #get rows corresponding workplace SIZE
        group_df <- Sector_SA2_df %>%
          filter(Firm_size == SIZE)
        
        #assign total count of workers who are employed in workplaces of that size to M
        M <- first(group_df$Count_people)
        #assign number of firms of that size to N
        N <- nrow(group_df)
        #We want to allocate the M number of workers across the N number of workplaces, resulting in the correct distribution
        
        #if that combination of SA2, SECTOR, and SIZE exist in our data..
        if(nrow(group_df)>0){
          #start sampling to fill firms with workers
          #since we have a criteria to meet, we will only close this iteration when the criteria is met.
          #until this, we will set a this to be false.
          criteria_met <- FALSE
          
          #count how many iterations, if this get's too high, we will adjust so the code does not get stuck.
          counter <- 0
          #create data_frame for storing vectors of possible workplace sizes.
          #We wanto to record all the guesses, so that if the counter gets too high we can take the closest effort and work with it.
          vec_storage <- data.frame(matrix(ncol=2+N,
                                           nrow=1, 
                                           dimnames=list(NULL,
                                                         c("Counter","Sum_vec",paste0("Cell",1:N))
                                           )
          ))
          #if we know the firm size (min and max are the same), then we can create the vector easy
          # M workers will be equal to N*workplace size. 
          if(first(group_df$min_size) == first(group_df$max_size)){
            vec <- group_df$min_size
            criteria_met <- TRUE
            #print("Firm size already known.")
          }
          
          while(criteria_met == FALSE){
            #add to counter
            counter <- counter+1
            
            #if there's only one firm in that group, assign value as the count of workers
            #(in these cases we don't need to distribute a count, since there's only 1 firm that can take the workers)
            if(N == 1){
              vec <- M[[1]]
              criteria_met <- TRUE
              #print("Criteria met straight away.")
            }else{ 
              # if there are more than 1 firm present, we will need to allocate the M count of people across the N count of workplaces.
              #first get the distribution for range of firm sizes in that group
              bag_Firm_sizes <- new_distribution %>%
                filter(between(Firm_size,first(group_df$min_size), first(group_df$max_size)))
              
              #sample from the distribution to create vector of firm sizes
              vec <- sample(bag_Firm_sizes$Firm_size, prob = bag_Firm_sizes$prob, size =N, replace = T)
              
              #store vector to main dataframe of vectors
              #this is where we store all of the guesses we make from that sample
              #if there have been many iterations (1000), we will stop sampling, and work with the closest guess.
              vec_storage<-rbind(vec_storage, c(counter,sum(vec),vec))
              
              #if the counter gets too high, we'll need to brute force a match using the best guess
              #we can do this by taking the vector that most closely sums to the count of workers, and perturbing the individual sizes
              if(counter == 100){
                #print("Going to shuffle sizes around using the best guess so far.")
                #print("Tried our best, now forcing the distribution to match")
                best_vec <- vec_storage %>%
                  #remove the test row made when creating the dataframe
                  filter(!is.na(Counter)) %>%
                  #calculate the difference between each the sum of each guess, and the actual number of workers needed to cover.
                  mutate(Difference = M-Sum_vec) %>%
                  #filter to vector with the minimum absolute difference between sum of sampled vector and count of workers
                  filter(abs(Difference) == min(abs(Difference))) %>%
                  #take the first result
                  head(1) %>%
                  #grab the vector
                  select(matches("Cell")) %>% unlist()
                
                Difference <- M-sum(best_vec)
                #for each individual discrepancy between that guess and the M number of workers,
                #we will add/substract a worker randomly from firms until no more difference
                for(. in seq_len(abs(Difference))){
                  best_vec[i] <- best_vec[i <- sample(N, 1)] + sign(Difference) #(this will +/- 1 to a random firm)
                }
                #while any firm size is negative or 0, we will reassign count to another firm
                
                #create a second counter to assess any issues running next section
                second_counter <- 0
                #if number of firms is greater than count of workers, there is going to be at least one firm allocated a size 0.
                if(N>M){
                  #print("shuffling to avoid negative values.")
                  while (any(best_vec < 0)) {
                    negs <- best_vec <= 0
                    pos  <- best_vec > 0
                    best_vec[negs][i] <- best_vec[negs][i <- sample(sum(negs), 1)] + 1
                    best_vec[pos][i]  <- best_vec[pos ][i <- sample(sum(pos ), 1)] - 1
                    second_counter <- second_counter+1
                    
                    #print warning if this while statement goes on too long
                    if(second_counter == 10000){print(paste("Stuck in a loop on",SA2,SECTOR,SIZE))}
                  }
                  #if the number of firms is less than the number of workers, we should be able to avoid firms sized 0.
                }else{
                  #print("shuffling to avoid zero counts")
                  #We could refine this even further by changing the conditional to ensure elements of best_vec are within correct range 
                  while (any(best_vec <= 0)) {
                    negs <- best_vec <= 0 #negs will now include 0
                    pos  <- best_vec > 1 #only sample from firms which can afford to drop one person
                    best_vec[negs][i] <- best_vec[negs][i <- sample(sum(negs), 1)] + 1
                    best_vec[pos][i]  <- best_vec[pos ][i <- sample(sum(pos ), 1)] - 1
                    
                    second_counter <- second_counter+1
                    #print warning if this while statement goes on too long
                    if(second_counter == 10000){print(paste("Stuck in a loop on",SA2,SECTOR,SIZE))}
                  }
                }
                
                vec <- best_vec
              }
              
              #if this vector of firm sizes matches the count - Success
              if(sum(vec)==M){
                #print("Criteria now met.")
                criteria_met <- TRUE}   #otherwise it will repeat the process
            }
          }
          
          # if the sum of the vec does not equal the expected size, print warning
          if(sum(vec)!=M|length(vec)!=N){print(paste0("Issue with: ",SA2,",",SECTOR,",",SIZE))}
          #append successful vector of firm sizes to main list
          allocated_Firm_size_df  <- bind_rows(allocated_Firm_size_df,group_df %>% mutate(allocated_Firm_size = vec))
        }
      }
    }
    
    #print summary at every 10% complete
    # if(SA2_i%%round(SA2_length/100)==0){
    #   print(paste0("Loop ",SA2_i,": Finished SA2 ", SA2))}
    if(SA2_i%%round(SA2_length/10)==0){
      print(paste0("Loop ",SA2_i,": Finished SA2 ", SA2))
      print(paste0("Time elapsed: ", round(Sys.time()-Time1,2),
                   ". ", round((SA2_i/SA2_length)*100), " Percent complete."))
    }
    
  }
  
  print("Finished imputation of workplace sizes")
  
  #what's the sum of the new allocated_Firm_size? Does it equal sum(Count_people) in original data (excluding TA == NA)
  sum(allocated_Firm_size_df$allocated_Firm_size) == sum(workplaces_sector_sa2_imputed$Count_people[workplaces_sector_sa2_imputed$TA2018!="NA"]) #should be TRUE
  #check across sectors
  for(sector in unique(workplaces_sector_sa2_imputed$Sector_anzsic06)){
    Diff <- sum(workplaces_sector_sa2_imputed %>%
                  filter(Sector_anzsic06 == sector) %>% 
                  pull(Count_people)
    ) - sum(allocated_Firm_size_df %>% 
              filter(Sector_anzsic06 == sector) %>% 
              pull(allocated_Firm_size)
    )
    print(paste0("Sector ", sector,". Difference = ", Diff))
    
  }
  
  
  #check across TAs
  for(TA in unique(workplaces_sector_sa2_imputed$TA2018)){
    Diff <- sum(workplaces_sector_sa2_imputed %>%
                  filter(TA2018 == TA) %>% 
                  pull(Count_people)
    ) - sum(allocated_Firm_size_df %>% 
              filter(TA2018 == TA) %>% 
              pull(allocated_Firm_size)
    )
    print(paste0("TA ", TA,". Difference = ", Diff))
    
  }
  
  #how many allocated firm sizes fit the desired range
  allocated_Firm_size_df %>%
    mutate(fit_range = ifelse(allocated_Firm_size >=min_size & allocated_Firm_size <= max_size, TRUE,FALSE)) %>%
    summarise(range_test = sum(fit_range)/nrow(.))
  
  allocated_Firm_size_df %>%
    mutate(fit_range = ifelse(allocated_Firm_size >=min_size & allocated_Firm_size <= max_size, TRUE,FALSE)) %>%
    group_by(SA22018,Sector_anzsic06,Firm_size) %>%
    filter(any(fit_range == F))
  #
  
  #any size 0?
  allocated_Firm_size_df %>% filter(allocated_Firm_size==0)
  
  return(allocated_Firm_size_df)
  
}