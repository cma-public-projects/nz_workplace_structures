This folder contains data folders used to create the synthetic set of workplaces. 
This includes files extracted from the IDI (df_firmsizes_sector_sa2_safe.csv,df_firmsizes_nz_safe.csv, df_firmsizes_sector_TA.csv)
and files obtained from public sources (workplaces_employees_industry_sa2.csv obtained from NZ.Stat
